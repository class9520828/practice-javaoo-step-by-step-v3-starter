O(Objective):
+ I learned about Java's Stream API and realized that it is a more efficient development method. &nbsp;In my future studies, I will pay attention to the use of this more frequently.
+ In the afternoon, I learned the concepts of object-oriented programming. Understand the three characteristics of object encapsulation, inheritance, and polymorphism. &nbsp;And in class, learn to use inheritance and polymorphism to complete tasks with high flexibility. &nbsp;I think this high aggregation and low coupling development approach is something I should pay special attention to in my future studies.
+ Through recent days of learning, I have become more familiar with the syntax of Java, which has greatly helped improve the speed of programming.

R(Reflective):  
&emsp;&emsp;Fruitful

I(Interpretive):  
&emsp;&emsp;I am not very familiar with the characteristics of object-oriented programming and the various features of Class in Java, which requires further learning and gradual mastery. &nbsp;Meanwhile, IDEA is a powerful tool, and I will continue to practice using it in the future.

D(Decision):  
&emsp;&emsp;I will pay more attention to the understanding and use of OOP and Stream APIs in the future.
