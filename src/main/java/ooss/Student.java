package ooss;

public class Student extends Person {
    private Klass klass;
    private boolean isLeader;

    public Student(int id, String name, int age) {
        super(id, name, age);
        this.klass = null;
        this.isLeader = false;
    }

    @Override
    public String introduce() {
        String introduceString = String.format("My name is %s.", this.getName()) + " "
                + String.format("I am %d years old.", this.getAge()) + " "
                + "I am a student.";
        if (this.klass != null && !this.isLeader) {
            return introduceString + " " + String.format("I am in class %d.", this.getKlass().getNumber());
        } else if (this.isLeader) {
            return introduceString + " " + String.format("I am the leader of class %d.", this.getKlass().getNumber());
        } else {
            return introduceString;
        }

    }

    public void join(Klass klass) {
        klass.attach(this);
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        if (this.klass != null) {
            return this.klass.equals(klass);
        } else {
            return false;
        }
    }

    public Klass getKlass() {
        return this.klass;
    }

    public void assignedAsLeader() {
        this.isLeader = true;
    }

    public boolean getIsLeader() {
        return isLeader;
    }


}
