package ooss;

import java.util.ArrayList;
import java.util.List;

public class Klass {
    private final int number;
    private final List<Person> klassTeachers;
    private final List<Person> klassStudents;
    public Klass(int number) {
        this.number = number;
        this.klassTeachers = new ArrayList<>();
        this.klassStudents = new ArrayList<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Klass klass = (Klass) o;

        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return number;
    }

    public void assignLeader(Student student) {
        if (student.getKlass() == null) {
            System.out.print("It is not one of us.");
        } else if (this.number == student.getKlass().number) {
            student.assignedAsLeader();
            this.klassTeachers.forEach(teacher -> System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.",
                    teacher.getName(), this.number, student.getName()));
            this.klassStudents.forEach(tempStudent -> System.out.printf("I am %s, student of Class %d. I know %s become Leader.",
                    tempStudent.getName(), this.number, student.getName()));
        } else {
            System.out.print("It is not one of us.");
        }


    }

    public int getNumber() {
        return number;
    }

    public boolean isLeader(Student student) {
        if (student.getKlass() == null) {
            return false;
        } else return (this.number == student.getKlass().number) && student.getIsLeader();

    }
    public void attach(Person person) {
        if (person instanceof Student) {
            this.klassStudents.add(person);
        } else {
            this.klassTeachers.add(person);
        }
    }

}
