package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {

    private final List<Klass> teachingKlass;

    public Teacher(int id, String name, int age) {
        super(id, name, age);
        this.teachingKlass = new ArrayList<>();
    }

    @Override
    public String introduce() {
        StringBuilder introduceString = new StringBuilder(String.format("My name is %s.", this.getName()) + " "
                + String.format("I am %d years old.", this.getAge()) + " "
                + "I am a teacher.");
        if (this.teachingKlass.size() != 0) {
            introduceString.append(" ").append("I teach Class ");
            for (int i = 0; i < this.teachingKlass.size(); i++) {
                if (i + 1 == this.teachingKlass.size()) {
                    introduceString.append(this.teachingKlass.get(i).getNumber()).append(".");
                } else {
                    introduceString.append(this.teachingKlass.get(i).getNumber()).append(", ");
                }
            }
        }
        return introduceString.toString();

    }

    public void assignTo(Klass klass) {
        this.teachingKlass.add(klass);
        klass.attach(this);
        introduce();
    }

    public boolean belongsTo(Klass klass){
        return this.teachingKlass.stream().anyMatch(tempKlass -> tempKlass.equals(klass));
    }
    public boolean isTeaching(Student student) {
        return this.teachingKlass.stream().anyMatch(tempKlass -> tempKlass.getNumber() == (student.getKlass().getNumber()));

    }


}
